## My st terminal
### Included patches :
- Scroll.
- Del key.
- Transparent.
- Emoji enabled.
    - Required fonts:
        - Symbola.
        - Inconsolata for Powerline.
- Copy and Highlight URL (Alt + u).

